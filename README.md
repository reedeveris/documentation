**Updates for Fall 2023** 

The Documentation project should have the following organization:
* The README.MD should contain:
  * A **brief** "Status" section that contains an indication of the current status of the project.  This should be updated every time there is a major change of functionality. 
  * A "Setting up the Dev Environment" section that includes instructions on how to set up the dev environment. Fine to use GitPod. 
  * A "Running the Application" section that includes instructions on how to run the full application. 
* A project titled "Requirements" that will hold the project requirements.
* A project titled "Design" that will hold all design documents. 
* Figure out if the information below the "END COMMENT" is required and if so, where it should be located.
* Figure out the proper location of all other material in this project.  
**END COMMENT**


```
How do they want to be able to view the current inventory? - Connor B
How often do they want to be able to view the inventory? - Connor B
Do they care about the current number of items? (I remember her mentioning that she only really cared about whether they had a sufficent amount of items on hand, but I could be misremembering.) - Connor B
If the answer to the above question is no, what's the threshold for a sufficent amount? - Connor B
Are student workers looking at the inventory aswell? - Connor B
Is there a specific format they want to see the inventory in? - Connor B
```
### Cameron Gowers Questions for Kolu Sharpe
- As a student who has used food pantries, would it be in the best interests for the school to evaluate scholarships people get and how many go to the pantry? Can help people..?

- In the inventory sheet I see an issue with Category names. I do not feel as if these are named well. As maybe a more unique and separated identifier as we have Canned Goods and Canned Soups, I do not think it should matter. Can we have better named categories? Keep things simple, school supplies, hygeine, canned goods, box goods, fruits, veggies, meat, etc.
- Would you mind counting all items even if their is an abundance?
    - For 
        - "Theft"
        - Need tracking Reporting

- For the gift card sheet why are you writing down the gift card number? 
    - People can be using these from this information? Taking away from the student in need? 
        For future prevent.

- In the guest log can we remove some columns? Is all information necessary?

- Can instead of giving someone a new card every time can we tell them to remember the id or give a phone number association? Since technically a student doesnt know IDs and the specific IDs graduation year and resident commutter status.
- What data is being collected and used from student workers? 
- Do you have any other artifacts or relavent information you have kept from us? 
- Would you instead prefer a paper prototype model to see how it will work?

