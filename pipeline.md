
#Purpose
The purpose of this document is to reference sources for CI/CD pipelines within Gitlab for the LFP project. 

#References
LFP Based 
    https://librefoodpantry.org/docs/ci-cd

Documentation on Gitlab
	General
		https://docs.gitlab.com/ee/ci/

Pipeline Tutorial Steps
	https://docs.gitlab.com/ee/ci/quick_start/
	
Docker Integration
	https://docs.gitlab.com/ee/ci/docker/

	Two Ways to do so
		https://docs.gitlab.com/ee/ci/docker/using_docker_images.html
		https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

Note Projects CI/CD is on by default so if for some reason it is not working someone may mess with project settings at one point.

External Documentation
 - https://www.howtogeek.com/devops/how-to-build-docker-images-in-a-gitlab-ci-pipeline/
 - https://www.youtube.com/watch?v=-CyVpfDQAG0

Gitlab CI / CD Templates 
 - https://docs.gitlab.com/ee/ci/examples/
Gitlab Docker Template
 - https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Docker.gitlab-ci.yml

#What I did for the gitlab pipeline during the spike. 

After I spoke with Professor Jackson, he referenced that he would hope all projects in the future would use a specific tool. I have referenced this in my pipeline to let that pipeline build ours. Any time the pipeline fails we should be referencing 
https://gitlab.com/LibreFoodPantry/common-services/tools/pipeline



